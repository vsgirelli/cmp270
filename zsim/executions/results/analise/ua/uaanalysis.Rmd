---
title: "Análise da simulação de aplicações paralelas com Zsim - UA"
author: "Valéria S. Girelli"
date: "July 25, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---


```{r}
setwd("/home/vsgirelli/dev/cmp270/zsim/executions/results/analise/ua")
library(ggplot2)
library(readr)
library(magrittr)
library(plyr)
library(dplyr)
library(latex2exp)
library(gridExtra)
library(grid)
library(gtable)
```


```{r}
all <- read.csv("../allCyclesMissesMeans.csv")
ua <- data.frame(all[grep(c("ua"), all$app), ])

# ua
ua$sim_avg <-(ua$sim_avg)/(10^11)
ua$sim_sd <- (ua$sim_sd)/(10^11)
ua$exec_avg <- (ua$exec_avg)/(10^11)
ua$exec_sd <- (ua$exec_sd)/(10^11)

ua$sim_avg_misses <-(ua$sim_avg_misses)/(10^9)
ua$sim_sd_misses <- (ua$sim_sd_misses)/(10^9)
ua$exec_avg_misses <- (ua$exec_avg_misses)/(10^9)
ua$exec_sd_misses <- (ua$exec_sd_misses)/(10^9)

```


```{r} 
# preparing uaM
uaM <- select(ua, c(threads, exec_avg, sim_avg))
uaM <- reshape2::melt(uaM, id.var='threads')
names(uaM)[2] <- "Execução"
names(uaM)[3] <- "Cycles"
uaM$Execução <- revalue(uaM$Execução, c("exec_avg" = "Real ", "sim_avg" = "Simulação"))
uaM$event <- "Ciclos"

uaMissesM <- select(ua, c(threads, exec_avg_misses, sim_avg_misses))
uaMissesM <- reshape2::melt(uaMissesM, id.var='threads')
names(uaMissesM)[2] <- "Execução"
names(uaMissesM)[3] <- "Misses"
uaMissesM$Execução <- revalue(uaMissesM$Execução, c("exec_avg_misses" = "Real ", "sim_avg_misses" = "Simulação"))
uaMissesM$event <- "Misses"

# putting all information together
uaM <- merge(uaM, uaMissesM, by = c("threads", "Execução"))

ua$threads <- as.character(ua$threads)

```


```{r} 
uacycles <- ggplot(data = uaM, aes(x=factor(threads), y=Cycles, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Ciclos (10^{11})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
uacycles

uamisses <- ggplot(data = uaM, aes(x=factor(threads), y=Misses, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Misses (10^{9})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
uamisses

```

```{r}
pdf(file = "uacycles.pdf", width = 8, height = 6)
uacycles
dev.off()

pdf(file = "uamisses.pdf", width = 8, height = 6)
uamisses
dev.off()
``` 


```{r}
all <- read.csv("../allCoherence.csv")
ua <- data.frame(all[grep(c("ua"), all$app), ])
```


```{r} 
# preparing uaM
ua$sim_rfo_to_llc <- (ua$sim_rfo_to_llc)/(10^8)
ua$exec_rfo_to_llc_avg <- (ua$exec_rfo_to_llc_avg)/(10^8)
```


```{r}
uaM <- select(ua, c(threads, exec_rfo_to_llc_avg, sim_rfo_to_llc))
uaM <- reshape2::melt(uaM, id.var='threads')
names(uaM)[2] <- "Execução"
names(uaM)[3] <- "Messages"
uaM$Execução <- revalue(uaM$Execução, c("exec_rfo_to_llc_avg" = "Real ", "sim_rfo_to_llc" = "Simulação"))
uaM$event <- "RFO"

uaM$threads <- as.character(ua$threads)
```


```{r}
uacoherence <- ggplot(data = uaM, aes(x=factor(threads), y=Messages, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('RFOs (10^{8})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
uacoherence
```


```{r}
pdf(file = "uacoherence.pdf", width = 8, height = 6)
uacoherence
dev.off()
``` 