---
title: "Análise da simulação de aplicações paralelas com Zsim - FT"
author: "Valéria S. Girelli"
date: "July 25, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---


```{r}
setwd("/home/vsgirelli/dev/cmp270/zsim/executions/results/analise/ft")
library(ggplot2)
library(readr)
library(magrittr)
library(plyr)
library(dplyr)
library(latex2exp)
library(gridExtra)
library(grid)
library(gtable)
```


```{r}
all <- read.csv("../allCyclesMissesMeans.csv")
ft <- data.frame(all[grep(c("ft"), all$app), ])

# ft
ft$sim_avg <-(ft$sim_avg)/(10^10)
ft$sim_sd <- (ft$sim_sd)/(10^10)
ft$exec_avg <- (ft$exec_avg)/(10^10)
ft$exec_sd <- (ft$exec_sd)/(10^10)

ft$sim_avg_misses <-(ft$sim_avg_misses)/(10^8)
ft$sim_sd_misses <- (ft$sim_sd_misses)/(10^8)
ft$exec_avg_misses <- (ft$exec_avg_misses)/(10^8)
ft$exec_sd_misses <- (ft$exec_sd_misses)/(10^8)

```


```{r} 
# preparing ftM
ftM <- select(ft, c(threads, exec_avg, sim_avg))
ftM <- reshape2::melt(ftM, id.var='threads')
names(ftM)[2] <- "Execução"
names(ftM)[3] <- "Cycles"
ftM$Execução <- revalue(ftM$Execução, c("exec_avg" = "Real ", "sim_avg" = "Simulação"))
ftM$event <- "Ciclos"

ftMissesM <- select(ft, c(threads, exec_avg_misses, sim_avg_misses))
ftMissesM <- reshape2::melt(ftMissesM, id.var='threads')
names(ftMissesM)[2] <- "Execução"
names(ftMissesM)[3] <- "Misses"
ftMissesM$Execução <- revalue(ftMissesM$Execução, c("exec_avg_misses" = "Real ", "sim_avg_misses" = "Simulação"))
ftMissesM$event <- "Misses"

# putting all information together
ftM <- merge(ftM, ftMissesM, by = c("threads", "Execução"))

ft$threads <- as.character(ft$threads)

```


```{r} 
ftcycles <- ggplot(data = ftM, aes(x=factor(threads), y=Cycles, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Ciclos (10^{10})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
ftcycles

ftmisses <- ggplot(data = ftM, aes(x=factor(threads), y=Misses, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Misses (10^{8})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 13)) +
  scale_y_discrete(limits = c(0, 2.5, 5, 7.5, 10, 12.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1) 
ftmisses

```

```{r}
pdf(file = "ftcycles.pdf", width = 8, height = 6)
ftcycles
dev.off()

pdf(file = "ftmisses.pdf", width = 8, height = 6)
ftmisses
dev.off()
```



```{r}
all <- read.csv("../allCoherence.csv")
ft <- data.frame(all[grep(c("ft"), all$app), ])
```


```{r} 
# preparing ftM
ft$sim_rfo_to_llc <- (ft$sim_rfo_to_llc)/(10^8)
ft$exec_rfo_to_llc_avg <- (ft$exec_rfo_to_llc_avg)/(10^8)
```


```{r}
ftM <- select(ft, c(threads, exec_rfo_to_llc_avg, sim_rfo_to_llc))
ftM <- reshape2::melt(ftM, id.var='threads')
names(ftM)[2] <- "Execução"
names(ftM)[3] <- "Messages"
ftM$Execução <- revalue(ftM$Execução, c("exec_rfo_to_llc_avg" = "Real ", "sim_rfo_to_llc" = "Simulação"))
ftM$event <- "RFO"

ftM$threads <- as.character(ft$threads)
```


```{r}
ftcoherence <- ggplot(data = ftM, aes(x=factor(threads), y=Messages, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('RFOs (10^{8})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1) 
ftcoherence
```


```{r}
pdf(file = "ftcoherence.pdf", width = 8, height = 6)
ftcoherence
dev.off()
``` 