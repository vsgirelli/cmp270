---
title: "Análise da simulação de aplicações paralelas com Zsim - BT"
author: "Valéria S. Girelli"
date: "July 25, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---


```{r}
setwd("/home/vsgirelli/dev/cmp270/zsim/executions/results/analise/bt")
library(ggplot2)
library(readr)
library(magrittr)
library(plyr)
library(dplyr)
library(latex2exp)
library(gridExtra)
library(grid)
library(gtable)
```


```{r}
all <- read.csv("../allCyclesMissesMeans.csv")
bt <- data.frame(all[grep(c("bt"), all$app), ])

# bt
bt$sim_avg <-(bt$sim_avg)/(10^11)
bt$sim_sd <- (bt$sim_sd)/(10^11)
bt$exec_avg <- (bt$exec_avg)/(10^11)
bt$exec_sd <- (bt$exec_sd)/(10^11)

bt$sim_avg_misses <-(bt$sim_avg_misses)/(10^9)
bt$sim_sd_misses <- (bt$sim_sd_misses)/(10^9)
bt$exec_avg_misses <- (bt$exec_avg_misses)/(10^9)
bt$exec_sd_misses <- (bt$exec_sd_misses)/(10^9)

```


```{r} 
# preparing btM
btM <- select(bt, c(threads, exec_avg, sim_avg))
btM <- reshape2::melt(btM, id.var='threads')
names(btM)[2] <- "Execução"
names(btM)[3] <- "Cycles"
btM$Execução <- revalue(btM$Execução, c("exec_avg" = "Real ", "sim_avg" = "Simulação"))
btM$event <- "Ciclos"

btMissesM <- select(bt, c(threads, exec_avg_misses, sim_avg_misses))
btMissesM <- reshape2::melt(btMissesM, id.var='threads')
names(btMissesM)[2] <- "Execução"
names(btMissesM)[3] <- "Misses"
btMissesM$Execução <- revalue(btMissesM$Execução, c("exec_avg_misses" = "Real ", "sim_avg_misses" = "Simulação"))
btMissesM$event <- "Misses"

# putting all information together
btM <- merge(btM, btMissesM, by = c("threads", "Execução"))

bt$threads <- as.character(bt$threads)

```


```{r} 
btcycles <- ggplot(data = btM, aes(x=factor(threads), y=Cycles, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Ciclos (10^{11})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
btcycles

btmisses <- ggplot(data = btM, aes(x=factor(threads), y=Misses, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Misses (10^{9})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 8)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
btmisses

```

```{r}
pdf(file = "btcycles.pdf", width = 8, height = 6)
btcycles
dev.off()

pdf(file = "btmisses.pdf", width = 8, height = 6)
btmisses
dev.off()
```


```{r}
all <- read.csv("../allCoherence.csv")
bt <- data.frame(all[grep(c("bt"), all$app), ])
```


```{r} 
# preparing btM
bt$sim_rfo_to_llc <- (bt$sim_rfo_to_llc)/(10^8)
bt$exec_rfo_to_llc_avg <- (bt$exec_rfo_to_llc_avg)/(10^8)
```


```{r}
btM <- select(bt, c(threads, exec_rfo_to_llc_avg, sim_rfo_to_llc))
btM <- reshape2::melt(btM, id.var='threads')
names(btM)[2] <- "Execução"
names(btM)[3] <- "Messages"
btM$Execução <- revalue(btM$Execução, c("exec_rfo_to_llc_avg" = "Real ", "sim_rfo_to_llc" = "Simulação"))
btM$event <- "RFO"

btM$threads <- as.character(bt$threads)
```


```{r}
btcoherence <- ggplot(data = btM, aes(x=factor(threads), y=Messages, fill=Execução, width = 0.5)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('RFOs (10^{8})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=18),
                                  axis.title=element_text(size=18),
                                  aspect.ratio = 1.3/1)
btcoherence
```


```{r}
pdf(file = "btcoherence.pdf", width = 8, height = 6)
btcoherence
dev.off()
``` 


```{r}
btplot <- grid.arrange(grobs = list(btcycles, btmisses, btcoherence), nrows=1, ncol=3)
```
```{r}
pdf(file = "btplot.pdf", width = 15, height = 6)
btplot
dev.off()
```

