SINUCA="/home/users/vsgirelli/cmp270/sinuca/"
export PIN_ROOT="$SINUCA/trace_generator"
export PINPLAY_HOME="$PIN_ROOT/extras/pinplay"
export PINPLAY_INCLUDE_HOME="$PINPLAY_HOME/include"
export PINPLAY_LIB_HOME="$PINPLAY_HOME/lib"
export EXT_LIB_HOME="$PINPLAY_HOME/lib-ext"
export PATH=$PIN_ROOT:$PINPLAY_HOME:$PINPLAY_INCLUDE_HOME:$PINPLAY_LIB_HOME:$EXT_LIB_HOME:$PATH
