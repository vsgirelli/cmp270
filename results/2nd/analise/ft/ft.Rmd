---
title: "Gráficos com prefetch, sem prefetch, e simulação - ft"
author: "Valéria S. Girelli"
date: "August 17, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---

```{r}
setwd("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/ft")
library(ggplot2)
library(readr)
library(magrittr)
library(plyr)
library(dplyr)
library(latex2exp)
library(gridExtra)
library(grid)
library(gtable)
```


Plots for each of the applications
```{r}
cycles <- read.csv("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/merged_cycles.csv")
l1dcache <- read.csv("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/merged_l1dcache.csv")
llc <- read.csv("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/merged_llc.csv")
```


```{r}
ftcycles <- data.frame(cycles[grep(c("ft"), cycles$app), ])
ftl1dcache <- data.frame(l1dcache[grep(c("ft"), l1dcache$app), ])
ftllc <- data.frame(llc[grep(c("ft"), llc$app), ])

ftcycles$app <- revalue(ftcycles$app, c("ft - 1" = "1", "ft - 2" = "2", "ft - 4" = "4", "ft - 8" = "8"))
names(ftcycles)[1] <- "threads"
ftl1dcache$app <- revalue(ftl1dcache$app, c("ft - 1" = "1", "ft - 2" = "2", "ft - 4" = "4", "ft - 8" = "8"))
names(ftl1dcache)[1] <- "threads"
ftllc$app <- revalue(ftllc$app, c("ft - 1" = "1", "ft - 2" = "2", "ft - 4" = "4", "ft - 8" = "8"))
names(ftllc)[1] <- "threads"
```

```{r}
ftcycles$value <- (ftcycles$value)/(10^10)
ftl1dcache$value <- (ftl1dcache$value)/(10^9)
ftllc$value <- (ftllc$value)/(10^8)
```


```{r}
ftc <- ggplot(ftcycles, aes(x = as.factor(threads), y = value, fill=Ciclos, width = 0.7)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Ciclos (10^{10})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 1)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=26),
                                  axis.title=element_text(size=26),
                                  aspect.ratio = 1.3/1)
ftc
```


```{r}
ftl1 <- ggplot(ftl1dcache, aes(x = as.factor(threads), y = value, fill=L1Dados, width = 0.7)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('L1Dcache (10^{9})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 1.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=26),
                                  axis.title=element_text(size=26),
                                  aspect.ratio = 1.3/1)
ftl1
```


```{r}
ftl <- ggplot(ftllc, aes(x = as.factor(threads), y = value, fill=LLC, width = 0.7)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('LLC (10^{8})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 4.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=26),
                                  axis.title=element_text(size=26),
                                  aspect.ratio = 1.3/1)
ftl
```


```{r}
pdf(file = "ftcycles.pdf", width = 8, height = 6)
ftc
dev.off()
``` 



```{r}
pdf(file = "ftl1dcache.pdf", width = 8, height = 6)
ftl1
dev.off()
``` 



```{r}
pdf(file = "ftllc.pdf", width = 8, height = 6)
ftl
dev.off()
``` 

