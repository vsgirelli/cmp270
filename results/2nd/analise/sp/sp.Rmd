---
title: "Gráficos com prefetch, sem prefetch, e simulação - sp"
author: "Valéria S. Girelli"
date: "August 17, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---

```{r}
setwd("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/sp")
library(ggplot2)
library(readr)
library(magrittr)
library(plyr)
library(dplyr)
library(latex2exp)
library(gridExtra)
library(grid)
library(gtable)
```


Plots for each of the applications
```{r}
cycles <- read.csv("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/merged_cycles.csv")
l1dcache <- read.csv("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/merged_l1dcache.csv")
llc <- read.csv("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/merged_llc.csv")
```


```{r}
spcycles <- data.frame(cycles[grep(c("sp"), cycles$app), ])
spl1dcache <- data.frame(l1dcache[grep(c("sp"), l1dcache$app), ])
spllc <- data.frame(llc[grep(c("sp"), llc$app), ])

spcycles$app <- revalue(spcycles$app, c("sp - 1" = "1", "sp - 2" = "2", "sp - 4" = "4", "sp - 8" = "8"))
names(spcycles)[1] <- "threads"
spl1dcache$app <- revalue(spl1dcache$app, c("sp - 1" = "1", "sp - 2" = "2", "sp - 4" = "4", "sp - 8" = "8"))
names(spl1dcache)[1] <- "threads"
spllc$app <- revalue(spllc$app, c("sp - 1" = "1", "sp - 2" = "2", "sp - 4" = "4", "sp - 8" = "8"))
names(spllc)[1] <- "threads"
```

```{r}
spcycles$value <- (spcycles$value)/(10^11)
spl1dcache$value <- (spl1dcache$value)/(10^10)
spllc$value <- (spllc$value)/(10^9)
```


```{r}
spc <- ggplot(spcycles, aes(x = as.factor(threads), y = value, fill=Ciclos, width = 0.7)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('Ciclos (10^{11})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 2)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=26),
                                  axis.title=element_text(size=26),
                                  aspect.ratio = 1.3/1)
spc
```


```{r}
spl1 <- ggplot(spl1dcache, aes(x = as.factor(threads), y = value, fill=L1Dados, width = 0.7)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('L1Dcache (10^{10})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 1.5)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=26),
                                  axis.title=element_text(size=26),
                                  aspect.ratio = 1.3/1)
spl1
```


```{r}
spl <- ggplot(spllc, aes(x = as.factor(threads), y = value, fill=LLC, width = 0.7)) + 
  geom_bar(stat = "identity", position = "dodge") +
  ylab(TeX('LLC (10^{9})$')) + xlab("Threads") +
  expand_limits(x = 0, y = c(0, 6)) +
  theme_bw() +
  theme(legend.position = "top", legend.title = element_blank(),
                                  legend.text = element_text(size = 16),
                                  axis.text=element_text(size=26),
                                  axis.title=element_text(size=26),
                                  aspect.ratio = 1.3/1)
spl
```


```{r}
pdf(file = "spcycles.pdf", width = 8, height = 6)
spc
dev.off()
``` 



```{r}
pdf(file = "spl1dcache.pdf", width = 8, height = 6)
spl1
dev.off()
``` 



```{r}
pdf(file = "spllc.pdf", width = 8, height = 6)
spl
dev.off()
``` 

