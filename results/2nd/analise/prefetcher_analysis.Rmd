---
title: "Análise das execuções reais"
author: "Valéria S. Girelli"
date: "June 27, 2019"
output:
  pdf_document: default
  word_document: default
  html_document: default
---

```{r}
setwd("/home/vsgirelli/dev/bolsa/cmp270/results/2nd/analise/")
library(ggplot2)
library(readr)
library(magrittr)
library(plyr)
library(dplyr)
```

Analysing results per core with prefetch enabled
```{r}
# merging all csv
filenames <- list.files("/home/vsgirelli/dev/cmp270/results/prefetch/", pattern="*.csv", full.names=TRUE) 

merged_csvs_pref <- read.csv(text = "app,class,threads,event,value")

for (file in filenames) {
  tmp <- read.csv(file, header = F, sep = ",")
  merged_csvs_pref <- rbind(merged_csvs_pref, tmp)
  rm(tmp)
}

names(merged_csvs_pref)[1] <-"app"
names(merged_csvs_pref)[2] <-"class"
names(merged_csvs_pref)[3] <-"threads"
names(merged_csvs_pref)[4] <-"resource"
names(merged_csvs_pref)[5] <-"event"
names(merged_csvs_pref)[6] <-"value"

write.csv(merged_csvs_pref, file = "merged_csvs_pref.csv", sep = ",", col.names = T, row.names = F)
```


Taking the mean of the 10 executions
```{r}
merged_csvs_pref <- read.csv("merged_csvs_pref.csv")
grouped <- merged_csvs_pref %>%
  group_by(app,threads,resource,event) %>%
  dplyr::summarize(avg = mean(value), sd = sd(value)*3/sqrt(n())) 

write.csv(grouped, file = "means_exec_pref_pref.csv", sep = ",", col.names = T, row.names = F)
```

Read means_exec_pref
```{r}
means_exec_pref <- read.csv("means_exec_pref.csv")

# max cycles 
cycles <- means_exec_pref %>%
  filter(event == "cycles") %>%
  group_by(app,threads) %>%
  dplyr::summarize(max(avg))
names(cycles)[3] <- "cycles"

# means L1
l1 <- means_exec_pref[grep(c("L1"), means_exec_pref$event), ]
l1 <- l1 %>%
  group_by(app,threads) %>%
  dplyr::summarize(sum = sum(avg))

l1$L1 <- (l1$sum)/(as.numeric(l1$threads)) 
l1 <- select(l1, -c(sum))
names(l1)[3] <- "L1-Dcache"

# sum LLC
llc <- means_exec_pref[grep(c("LLC"), means_exec_pref$event), ]
llc <- llc %>%
  group_by(app,threads) %>%
  dplyr::summarize(sum = sum(avg))
names(llc)[3] <- "LLC"


# merging all together
aux <- merge(cycles, l1,  by = c("app", "threads"))
allExec <- merge(aux, llc,  by = c("app", "threads"))
write.csv(allExec, file = "all_exec_pref.csv", sep = ",", col.names = T, row.names = F)
```
